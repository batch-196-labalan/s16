// alert("hello");

// ARITHMETIC OPERATORS

let x = 1297;
let y = 7831;

let sum = x + y;
console.log("Addition operator: " + sum);

let difference = y - x;
console.log("Subtraction operator: " + difference);

let product = x * y;
console.log("Multiplication operator: " + product);

let quotient = x / y;
console.log("Division operator: " + quotient);

let remainder = y % x;
console.log("Modulo operator: " + remainder);



// ASSIGNMENT OPERATOR (=)

let assignmentNumber = 8;

	// Addition assignment operator (+=)
assignmentNumber += 2;
console.log("Addition Assignment " + assignmentNumber);

assignmentNumber += 2;
console.log("Addition Assignment " + assignmentNumber);

assignmentNumber -= 2;
console.log("Subtraction Assignment " + assignmentNumber);

assignmentNumber *= 2;
console.log("Multiplication Assignment " + assignmentNumber);

assignmentNumber /= 2;
console.log("Division Assignment " + assignmentNumber);

	// string addition assignment
	let string1 = "Boston";
	let string2 = "celtics";
	string1 += string2;
	console.log(string1);


// MULTIPLE OPERATORS & PARENTHESIS

let mdas = 1 + 2 - 3 * 4 / 5;
console.log(mdas);

let pemdas = 1 + (2 -3) * (4 / 5);
console.log("PEMDAS: " + pemdas);



// INCREMENT & DECREMENT

   // starts with plus or minus 1
let z = 1;
let increment = ++z;
console.log("Pre-increment: " + increment);
console.log("Value of z: " + z);
  
  // post (go back to the current value)
increment = z++; 
console.log("Post-increment: " + increment);
console.log("Value of z: " + z);


let decrement = --z;
console.log("Pre-decrement:" + decrement);
console.log("Value of z: " + z);

decrement = z--;
console.log("Post-decrement:" + decrement);
console.log("Value of z: " + z);



// TYPE COERCION (not seen)

let numA = '10';
let numB = 12;

let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion);

let numC = 16;
let numD = 14;
let nonCoercion = numC + numD;
console.log(nonCoercion);
console.log(typeof nonCoercion);

// value of true is 1 false is 0
let numE = true + 1;
console.log(numE);

let numF = false + 1;
console.log(numF);



// EQUALITY OPERATOR (==) comparing (gives boolean value) coercion is also applicable here

console.log(1 == 1);
console.log(1 == 2);
console.log(1 == '1');
console.log(false == 0);
console.log('johnny' == 'johnny');
console.log('Johnny' == 'johnny');


// INEQUALITY OPERATOR (!=)

console.log(1 != 1);
console.log(1 != 2);



// STRICT EQUALITY OPERATOR (===) (should have same value and data type)
console.log(1 === 1);
console.log(1 === '1');
console.log(false === 0);

	let anne = 'ange';
	console.log('ange' === anne);

// STRICT INEQUALITY OPERATOR
console.log( 1 !== 1);
console.log( true !== 1);
console.log( true !== false);



// RELATIONAL OPERATORS (results to boolean value, type coercion still works here)
let a = 50;
let b = 65;

	// greater than (>)
let isGreaterThan = a > b;
console.log(isGreaterThan);

	// less than (<)
let isLessThan = a < b;
console.log(isLessThan);	

	// greater than or equal (>=)
let isGTE = a >= b;
console.log(isGTE);

	// less than or equal (>=)
let isLTE = a <= b;
console.log(isLTE);

		// true
let numStr = '30';
console.log(a > numStr);
 
 		// false
let str = 'twenty';
console.log(b >= str);



// LOGICAL OPERATORS (should have the same bolean value)
  // AND operator (&&)

let isAdmin = false;
let isRegistered = true;
let isLegalAge = true;

let authorization1 = isAdmin && isRegistered;
console.log(authorization1);

let authorization2 = isLegalAge && isRegistered;
console.log(authorization2);

let authorization3 = isLegalAge && isAdmin;
console.log(authorization3);

let random = isAdmin && false;
console.log(random);


let requiredLevel = 95;
let requiredAge = 18;


	// false
let authorization4 = isRegistered && requiredLevel === 25;
console.log(authorization4); 

	// true
let authorization5 = isRegistered && isLegalAge && requiredLevel === 95;
console.log(authorization5); 

let userName = 'gamer2022';
let userName2 = 'theTinker';
let userAge = 15;
let userAge2 = 26;

	// false
let registration1 = userName.length > 8 && userAge >= requiredAge;
console.log(registration1);

	// true
let registration2 = userName2.length > 8 && userAge2 >= requiredAge;
console.log(registration2);



	// OR OPERATOR ( || - DOUBLE PIPE)

let userLevel = 100;
let userLevel2 = 65;

	// false
let guildRequirement = isRegistered && userLevel >= requiredLevel && userAge >= requiredAge
console.log(guildRequirement);

	// true
let guildRequirement2 = isRegistered || userLevel2 >= requiredLevel && userAge2 >= requiredAge
console.log(guildRequirement2);

	// true
let guildRequirement3 = userLevel >= requiredLevel || userAge >= requiredAge;
console.log(guildRequirement3);

	// false
let guildAdmin = isAdmin || userLevel2 >= requiredLevel;
console.log(guildAdmin);


	// NOT OPERATOR (!)

	// false
console.log(!isRegistered); 


	// true
let opposite1 = !isAdmin;
console.log(opposite1);